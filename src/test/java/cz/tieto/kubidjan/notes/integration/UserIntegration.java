package cz.tieto.kubidjan.notes.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.tieto.kubidjan.notes.NotesApplication;
import cz.tieto.kubidjan.notes.model.User;
import cz.tieto.kubidjan.notes.repository.UserRepository;
import cz.tieto.kubidjan.notes.service.impl.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = NotesApplication.class)
@AutoConfigureMockMvc
@SpringBootTest
public class UserIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserServiceImpl userService;

    public User user;
    private Logger logger = LogManager.getLogger(UserIntegration.class);


    @Before
    public void setUp() {
        user = new User();
        user.setId(1);
        user.setFirstName("Shrek");
        user.setLastName("III");
        user.setPassword("notEmpty");

    }

    @Test
    public void saveUser_thenStatus200() {
        try {
            mockMvc.perform(post("/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(toJson(user)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"));

            User savedUser = userRepository.findById(1L).orElseThrow();
            assertThat(user.getId()).isEqualTo(savedUser.getId());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Test
    public void getUser_thenStatus200() {

        try {
            userRepository.save(user);

            mockMvc.perform(get("/users")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andExpect(jsonPath("$[0].id", is(1)));

        } catch (Exception e) {
            logger.error(e.getMessage());
            System.out.println("sadsd");
        }
    }


    public static String toJson(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

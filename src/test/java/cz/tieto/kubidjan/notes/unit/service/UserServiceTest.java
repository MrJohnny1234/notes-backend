package cz.tieto.kubidjan.notes.unit.service;

import cz.tieto.kubidjan.notes.dto.UserDTO;
import cz.tieto.kubidjan.notes.mapper.GroupMapperDTO;
import cz.tieto.kubidjan.notes.mapper.NoteMapperDTO;
import cz.tieto.kubidjan.notes.mapper.TagMapperDTO;
import cz.tieto.kubidjan.notes.mapper.UserMapperDTO;
import cz.tieto.kubidjan.notes.model.User;
import cz.tieto.kubidjan.notes.repository.UserRepository;
import cz.tieto.kubidjan.notes.service.impl.UserServiceImpl;
import cz.tieto.kubidjan.notes.unit.controller.UserControllerTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    private UserMapperDTO userMapperDTO;
    private PasswordEncoder passwordEncoder;
    private AuthenticationManager authenticationManager;

    @InjectMocks
    private UserServiceImpl userService;

    private Logger logger = LogManager.getLogger(UserControllerTest.class);


    @BeforeEach
    public void setUp() {
        TagMapperDTO tagMapperDTO = new TagMapperDTO();
        NoteMapperDTO noteMapperDTO = new NoteMapperDTO(tagMapperDTO);
        GroupMapperDTO groupMapperDTO = new GroupMapperDTO(noteMapperDTO, tagMapperDTO);
        userMapperDTO = new UserMapperDTO(groupMapperDTO, tagMapperDTO);
        userService = new UserServiceImpl(userRepository, userMapperDTO, passwordEncoder);
    }

    @Test
    void shouldBeSavedSuccessfully() {
        UserDTO userDTO = new UserDTO();
        User user = userMapperDTO.convertToEntity(userDTO);

        when(userRepository.save(any(User.class))).thenReturn(user);
        User savedUser = userService.registerNewUserAccount(userDTO);

        try {
            assertThat(savedUser).isNotNull();
            verify(userRepository).save(any(User.class));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Test
    void getUserByIdTest() {
        User user = new User();
        user.setId(1L);
        user.setLastName("Fiona");
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        try {
            UserDTO gottenUser = userService.getOne(1L);
            assertEquals(user.getId(), gottenUser.getId());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

}

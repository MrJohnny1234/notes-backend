package cz.tieto.kubidjan.notes.unit.controller;

import cz.tieto.kubidjan.notes.controller.UserController;
import cz.tieto.kubidjan.notes.dto.UserDTO;
import cz.tieto.kubidjan.notes.service.impl.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    private Logger logger = LogManager.getLogger(UserControllerTest.class);


    @Test
    void loadOne() {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("Shrek");

        try {
            when(userService.getOne(any(Long.TYPE))).thenReturn(userDTO);
            this.mockMvc.perform(get("/users/1")).andDo(print()).andExpect(status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Test
    void loadAll() {
        ArrayList<UserDTO> users = new ArrayList<UserDTO>();
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("Shrek");
        users.add(userDTO);

        try {
            when(userService.getAll()).thenReturn(users);
            this.mockMvc.perform(get("/users")).andDo(print()).andExpect(status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}

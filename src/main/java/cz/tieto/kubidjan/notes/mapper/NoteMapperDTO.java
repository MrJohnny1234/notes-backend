package cz.tieto.kubidjan.notes.mapper;

import cz.tieto.kubidjan.notes.dto.NoteDTO;
import cz.tieto.kubidjan.notes.dto.TagDTO;
import cz.tieto.kubidjan.notes.model.Note;
import cz.tieto.kubidjan.notes.model.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@RestController
public class NoteMapperDTO implements MapperDTO<Note, NoteDTO> {

    TagMapperDTO tagMapperDTO;

    @Override
    public NoteDTO convertToDto(Note note) {
        List<TagDTO> tagsDTO = new ArrayList<>();

        for (Tag tag : note.getTags()
        ) {
            tagsDTO.add(tagMapperDTO.convertToDto(tag));
        }

        return NoteDTO.builder()
                .id(note.getId())
                .name(note.getName())
                .text(note.getText())
                .lastChanged(note.getLastChanged())
                .tagDTOS(tagsDTO)
                .build();
    }

    @Override
    public Note convertToEntity(NoteDTO noteDTO) {
        List<Tag> tags = new ArrayList<>();

        for (TagDTO tagDTO : noteDTO.getTagDTOS()
        ) {
            tags.add(tagMapperDTO.convertToEntity(tagDTO));
        }

        return Note.builder()
                .id(noteDTO.getId())
                .name(noteDTO.getName())
                .text(noteDTO.getText())
                .tags(tags)
                .lastChanged(noteDTO.getLastChanged())
                .build();
    }
}

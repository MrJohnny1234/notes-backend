package cz.tieto.kubidjan.notes.mapper;

import cz.tieto.kubidjan.notes.dto.TagDTO;
import cz.tieto.kubidjan.notes.model.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class TagMapperDTO implements MapperDTO<Tag, TagDTO> {

    @Override
    public TagDTO convertToDto(Tag tag) {

        return TagDTO.builder()
                .id(tag.getId())
                .name(tag.getName())
                .lastChanged(tag.getLastChanged())
                .build();
    }

    @Override
    public Tag convertToEntity(TagDTO tagDTO) {

        return Tag.builder()
                .id(tagDTO.getId())
                .name(tagDTO.getName())
                .lastChanged(tagDTO.getLastChanged())
                .build();
    }

}

package cz.tieto.kubidjan.notes.mapper;

import cz.tieto.kubidjan.notes.dto.GroupDTO;
import cz.tieto.kubidjan.notes.dto.NoteDTO;
import cz.tieto.kubidjan.notes.dto.TagDTO;
import cz.tieto.kubidjan.notes.model.Group;
import cz.tieto.kubidjan.notes.model.Note;
import cz.tieto.kubidjan.notes.model.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@RestController
public class GroupMapperDTO implements MapperDTO<Group, GroupDTO> {

    NoteMapperDTO noteMapperDTO;
    TagMapperDTO tagMapperDTO;

    @Override
    public GroupDTO convertToDto(Group group) {
        List<NoteDTO> noteDTOS = new ArrayList<>();
        List<TagDTO> tagsDTO = new ArrayList<>();

        for (Note note : group.getNotes()
        ) {
            noteDTOS.add(noteMapperDTO.convertToDto(note));
        }

        for (Tag tag : group.getTags()
        ) {
            tagsDTO.add(tagMapperDTO.convertToDto(tag));
        }

        return GroupDTO.builder()
                .id(group.getId())
                .name(group.getName())
                .noteDTOS(noteDTOS)
                .tagDTOS(tagsDTO)
                .lastChanged(group.getLastChanged())
                .build();
    }

    @Override
    public Group convertToEntity(GroupDTO groupDTO) {
        List<Note> notes = new ArrayList<>();
        List<Tag> tags = new ArrayList<>();

        for (NoteDTO noteDTO : groupDTO.getNoteDTOS()
        ) {
            notes.add(noteMapperDTO.convertToEntity(noteDTO));
        }

        for (TagDTO tagDTO : groupDTO.getTagDTOS()
        ) {
            tags.add(tagMapperDTO.convertToEntity(tagDTO));
        }

        return Group.builder()
                .id(groupDTO.getId())
                .name(groupDTO.getName())
                .notes(notes)
                .tags(tags)
                .lastChanged(groupDTO.getLastChanged())
                .build();
    }
}

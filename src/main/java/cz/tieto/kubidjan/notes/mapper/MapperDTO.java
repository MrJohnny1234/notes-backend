package cz.tieto.kubidjan.notes.mapper;

public interface MapperDTO<T, K> {

    public K convertToDto(T t);

    public T convertToEntity(K k);
}

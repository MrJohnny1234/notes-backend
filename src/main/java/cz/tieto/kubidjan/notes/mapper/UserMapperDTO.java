package cz.tieto.kubidjan.notes.mapper;

import cz.tieto.kubidjan.notes.dto.GroupDTO;
import cz.tieto.kubidjan.notes.dto.TagDTO;
import cz.tieto.kubidjan.notes.dto.UserDTO;
import cz.tieto.kubidjan.notes.model.Group;
import cz.tieto.kubidjan.notes.model.Tag;
import cz.tieto.kubidjan.notes.model.User;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@RestController
public class UserMapperDTO implements MapperDTO<User, UserDTO> {

    GroupMapperDTO groupMapperDTO;
    TagMapperDTO tagMapperDTO;


    @Override
    public UserDTO convertToDto(User user) {
        List<GroupDTO> groupDTOS = new ArrayList<>();
        List<TagDTO> tagDTOS = new ArrayList<>();

        for (Group group : user.getGroups()
        ) {
            groupDTOS.add(groupMapperDTO.convertToDto(group));
        }

        for (Tag tag : user.getTags()
        ) {
            tagDTOS.add(tagMapperDTO.convertToDto(tag));
        }

        return UserDTO.builder()
                .id(user.getId())
                .role(user.getRole())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .password(user.getPassword())
                .lastChanged(user.getLastChanged())
                .groupDTOS(groupDTOS)
                .tagsDTO(tagDTOS)
                .username(user.getUsername())
                .build();
    }

    @Override
    public User convertToEntity(UserDTO userDTO) {

        List<Group> groups = new ArrayList<>();
        List<Tag> tags = new ArrayList<>();

        for (GroupDTO groupDTO : userDTO.getGroupDTOS()
        ) {
            groups.add(groupMapperDTO.convertToEntity(groupDTO));
        }

        for (TagDTO tagDTO : userDTO.getTagsDTO()
        ) {
            tags.add(tagMapperDTO.convertToEntity(tagDTO));
        }

        return User.builder()
                .id(userDTO.getId())
                .role(userDTO.getRole())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .password(userDTO.getPassword())
                .groups(groups)
                .tags(tags)
                .lastChanged(userDTO.getLastChanged())
                .username(userDTO.getUsername())
                .build();
    }
}

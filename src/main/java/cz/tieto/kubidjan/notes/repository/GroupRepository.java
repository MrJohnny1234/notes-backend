package cz.tieto.kubidjan.notes.repository;

import cz.tieto.kubidjan.notes.model.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {

}

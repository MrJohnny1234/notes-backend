package cz.tieto.kubidjan.notes.repository;

import cz.tieto.kubidjan.notes.model.Note;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {
}

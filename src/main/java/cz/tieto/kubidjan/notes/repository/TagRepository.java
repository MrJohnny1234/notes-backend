package cz.tieto.kubidjan.notes.repository;

import cz.tieto.kubidjan.notes.model.Tag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends CrudRepository<Tag, Long> {

}

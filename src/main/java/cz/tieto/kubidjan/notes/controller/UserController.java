package cz.tieto.kubidjan.notes.controller;

import cz.tieto.kubidjan.notes.config.HandleValidationExceptions;
import cz.tieto.kubidjan.notes.dto.UserDTO;
import cz.tieto.kubidjan.notes.model.UserDetailsImpl;
import cz.tieto.kubidjan.notes.service.impl.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class UserController extends HandleValidationExceptions {

    private final UserServiceImpl userServiceImpl;

    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @GetMapping(path = "/users", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<UserDTO> viewUsers() {
        return userServiceImpl.getAll();
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #id")
    @GetMapping(path = "/users/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    UserDTO viewUser(@PathVariable Long id, @AuthenticationPrincipal UserDetailsImpl authUser) {
        return userServiceImpl.getOne(id);
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #userDetails.getId()")
    @PutMapping(path = "/users", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> editUser(@Valid @RequestBody UserDTO userDetails, @AuthenticationPrincipal UserDetailsImpl authUser) {
        userServiceImpl.update(userDetails);
        return ResponseEntity.ok("User was updated successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #id")
    @DeleteMapping(path = "/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id, @AuthenticationPrincipal UserDetailsImpl authUser) {
        userServiceImpl.deleteOne(id);
        return ResponseEntity.ok("User was deleted successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @DeleteMapping(path = "/users")
    public ResponseEntity<String> deleteUsers() {
        userServiceImpl.deleteAll();
        return ResponseEntity.ok("Users were deleted successfully");
    }

    @PostMapping(path = "/users/register")
    public ResponseEntity<String> register(@Valid @RequestBody UserDTO userDetails) {
        userServiceImpl.registerNewUserAccount(userDetails);
        return ResponseEntity.ok("Users was registered successfully");
    }
}

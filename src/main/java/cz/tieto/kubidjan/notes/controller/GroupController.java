package cz.tieto.kubidjan.notes.controller;

import cz.tieto.kubidjan.notes.config.HandleValidationExceptions;
import cz.tieto.kubidjan.notes.dto.GroupDTO;
import cz.tieto.kubidjan.notes.model.UserDetailsImpl;
import cz.tieto.kubidjan.notes.service.impl.GroupServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class GroupController extends HandleValidationExceptions {

    private final GroupServiceImpl groupServiceImpl;

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @GetMapping(path = "/groups", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<GroupDTO> viewGroups(@RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        return groupServiceImpl.getAll(idUser);
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @GetMapping(path = "/groups/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    GroupDTO viewGroup(@PathVariable Long id, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        return groupServiceImpl.getOne(id, idUser);
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @PutMapping(path = "/groups", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> editGroup(@Valid @RequestBody GroupDTO groupDetails, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        groupServiceImpl.update(groupDetails, idUser);
        return ResponseEntity.ok("Group was updated successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @PostMapping(path = "/groups", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> createGroup(@Valid @RequestBody GroupDTO groupDetails, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        groupServiceImpl.saveOne(groupDetails, idUser);
        return ResponseEntity.ok("Group was added successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @DeleteMapping(path = "/groups/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> deleteNote(@PathVariable Long id, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        groupServiceImpl.deleteOne(id, idUser);
        return ResponseEntity.ok("Group was deleted successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @DeleteMapping(path = "/groups")
    public ResponseEntity<String> deleteGroups(@RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        groupServiceImpl.deleteAll(idUser);
        return ResponseEntity.ok("Groups were deleted successfully");
    }
}
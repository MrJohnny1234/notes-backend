package cz.tieto.kubidjan.notes.controller;

import cz.tieto.kubidjan.notes.config.HandleValidationExceptions;
import cz.tieto.kubidjan.notes.dto.NoteDTO;
import cz.tieto.kubidjan.notes.model.UserDetailsImpl;
import cz.tieto.kubidjan.notes.service.impl.NoteServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class NoteController extends HandleValidationExceptions {

    private final NoteServiceImpl noteServiceImpl;

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @GetMapping(path = "/notes", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<NoteDTO> viewNotes(@RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        return noteServiceImpl.getAll(idUser);
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @GetMapping(path = "/notes/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    NoteDTO viewNote(@PathVariable Long id, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        return noteServiceImpl.getOne(id, idUser);
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @PutMapping(path = "/notes", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> editNote(@Valid @RequestBody NoteDTO noteDetails, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        noteServiceImpl.update(noteDetails, idUser);
        return ResponseEntity.ok("Note was updated successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @PostMapping(path = "/notes", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> createNote(@Valid @RequestBody NoteDTO noteDetails, @RequestParam Long idUser, @RequestParam Long idGroup, @AuthenticationPrincipal UserDetailsImpl authUser) {
        noteServiceImpl.saveOne(noteDetails, idUser, idGroup);
        return ResponseEntity.ok("Note was added successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @DeleteMapping(path = "/notes/{id}")
    public ResponseEntity<String> deleteNote(@PathVariable Long id, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        noteServiceImpl.deleteOne(id, idUser);
        return ResponseEntity.ok("Note was deleted successfully");
    }
    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @DeleteMapping(path = "/notes")
    public ResponseEntity<String> deleteNotes(@RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        noteServiceImpl.deleteAll(idUser);
        return ResponseEntity.ok("Notes were deleted successfully");
    }
}
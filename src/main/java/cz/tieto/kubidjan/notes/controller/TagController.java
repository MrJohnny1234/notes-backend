package cz.tieto.kubidjan.notes.controller;

import cz.tieto.kubidjan.notes.dto.TagDTO;
import cz.tieto.kubidjan.notes.config.HandleValidationExceptions;
import cz.tieto.kubidjan.notes.model.UserDetailsImpl;
import cz.tieto.kubidjan.notes.service.impl.TagServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class TagController extends HandleValidationExceptions {

    private final TagServiceImpl tagServiceImpl;

    @GetMapping(path = "/tags", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<TagDTO> viewTags() {
        return tagServiceImpl.getAll();
    }

    @GetMapping(path = "/tags/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    TagDTO viewTag(@PathVariable Long id) {
        return tagServiceImpl.getOne(id);
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @PutMapping(path = "/tags", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> editTag(@Valid @RequestBody TagDTO tagDetails, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        tagServiceImpl.update(tagDetails, idUser);
        return ResponseEntity.ok("Tag was updated successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @PostMapping(path = "/tags", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> createTag(@Valid @RequestBody TagDTO tagDetails, @RequestParam Long idUser, @AuthenticationPrincipal UserDetailsImpl authUser) {
        tagServiceImpl.saveOne(tagDetails, idUser);
        return ResponseEntity.ok("Tag was added successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @DeleteMapping(path = "/tags/{id}")
    public ResponseEntity<String> deleteTag(@PathVariable Long id, @RequestParam Long idUser,  @AuthenticationPrincipal UserDetailsImpl authUser) {
         tagServiceImpl.deleteOne(id, idUser);
        return ResponseEntity.ok("Tags were deleted successfully");
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR') or #authUser.getId() == #idUser")
    @DeleteMapping(path = "/tags")
    public ResponseEntity<String> deleteTags(@RequestParam Long idUser,  @AuthenticationPrincipal UserDetailsImpl authUser) {
         tagServiceImpl.deleteAll(idUser);
        return ResponseEntity.ok("Tags were deleted successfully");
    }
}


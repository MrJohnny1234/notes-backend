package cz.tieto.kubidjan.notes.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@Entity
@Data
@NoArgsConstructor
public class Note {

    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String text;
    private LocalDateTime lastChanged;

    @ManyToMany(mappedBy = "notes", fetch = FetchType.LAZY)
    private List<Tag> tags = new ArrayList<>();

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Group group;

    @PrePersist
    @PreUpdate
    private void insertTime() {
        this.setLastChanged(LocalDateTime.now());
    }
}

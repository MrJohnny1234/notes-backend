package cz.tieto.kubidjan.notes.model;

public enum Roles {
    ADMINISTRATOR,
    VISITOR
}

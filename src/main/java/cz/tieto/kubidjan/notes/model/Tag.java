package cz.tieto.kubidjan.notes.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@Entity
@Data
@NoArgsConstructor
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private LocalDateTime lastChanged;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Note> notes = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Group> groups = new ArrayList<>();

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;

    @PrePersist
    @PreUpdate
    private void insertTime() {
        this.setLastChanged(LocalDateTime.now());
    }

}

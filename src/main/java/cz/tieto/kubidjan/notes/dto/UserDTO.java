package cz.tieto.kubidjan.notes.dto;

import cz.tieto.kubidjan.notes.model.Roles;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO {

    private long id;

    @NotEmpty
    @Size(min = 1, max = 40, message = "First name mush have 1-40 characters")
    private String firstName;

    @NotEmpty
    @Size(min = 1, max = 40, message = "Last name mush have 1-40 characters")
    private String lastName;

    @NotEmpty
    @Size(min = 6, message = "Password needs to have at least 6 characters")
    private String password;

    @NotEmpty
    private String username;


    @Null
    private Roles role;
    private LocalDateTime lastChanged;

    private List<GroupDTO> groupDTOS = new ArrayList<>();

    private List<TagDTO> tagsDTO = new ArrayList<>();


}

package cz.tieto.kubidjan.notes.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TagDTO {

    private long id;

    @NotEmpty
    @Size(min = 1, max = 40, message = "Name mush have 1-40 characters")
    private String name;
    private LocalDateTime lastChanged;
}

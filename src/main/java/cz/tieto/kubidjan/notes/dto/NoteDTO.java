package cz.tieto.kubidjan.notes.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NoteDTO {

    private long id;

    @NotEmpty
    @Size(min = 1, max = 40, message = "Name mush have 1-40 characters")
    private String name;

    @NotEmpty
    @Size(min = 1, message = "Note must have some text inside")
    private String text;
    private LocalDateTime lastChanged;
    private List<TagDTO> tagDTOS = new ArrayList<>();
}

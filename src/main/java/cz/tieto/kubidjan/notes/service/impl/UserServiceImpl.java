package cz.tieto.kubidjan.notes.service.impl;

import cz.tieto.kubidjan.notes.dto.UserDTO;
import cz.tieto.kubidjan.notes.mapper.UserMapperDTO;
import cz.tieto.kubidjan.notes.model.Roles;
import cz.tieto.kubidjan.notes.model.User;
import cz.tieto.kubidjan.notes.repository.UserRepository;
import cz.tieto.kubidjan.notes.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;
    private final UserMapperDTO userMapperDTO;
    private final PasswordEncoder passwordEncoder;


    public User update(UserDTO userDTO) {
        User user = userMapperDTO.convertToEntity(userDTO);
        User oldUser = userRepository.findById(user.getId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        oldUser.setFirstName(user.getFirstName());
        oldUser.setLastName(user.getLastName());
        oldUser.setPassword(user.getPassword());
        userRepository.save(oldUser);
        return oldUser;
    }

    public void deleteOne(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));
        if (user.getId() != id) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This is not you");
        } else {
            userRepository.delete(user);
        }
    }

    public void deleteAll() {
        userRepository.deleteAll();
    }

    public UserDTO getOne(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));
        if (user.getId() != id) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This is not you");
        } else {
            return userMapperDTO.convertToDto(user);
        }
    }

    public List<UserDTO> getAll() {
        List<UserDTO> lst = new ArrayList<>();

        for (User i : userRepository.findAll()) {
            lst.add(userMapperDTO.convertToDto(i));
        }

        return lst;
    }

    public User registerNewUserAccount(UserDTO userDTO) {
        if (userRepository.findByUsername(userDTO.getUsername()) != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This UserName is already taken.");
        }

        userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userDTO.setRole(Roles.ADMINISTRATOR);
        return userRepository.save(userMapperDTO.convertToEntity(userDTO));
    }

}

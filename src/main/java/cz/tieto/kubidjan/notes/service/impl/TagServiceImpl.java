package cz.tieto.kubidjan.notes.service.impl;

import cz.tieto.kubidjan.notes.dto.TagDTO;
import cz.tieto.kubidjan.notes.mapper.TagMapperDTO;
import cz.tieto.kubidjan.notes.mapper.UserMapperDTO;
import cz.tieto.kubidjan.notes.model.Group;
import cz.tieto.kubidjan.notes.model.Note;
import cz.tieto.kubidjan.notes.model.Tag;
import cz.tieto.kubidjan.notes.model.User;
import cz.tieto.kubidjan.notes.repository.TagRepository;
import cz.tieto.kubidjan.notes.service.TagService;
import cz.tieto.kubidjan.notes.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;
    private final TagMapperDTO tagMapperDTO;
    private final UserMapperDTO userMapperDTO;
    private final UserService userService;

    public Tag update(TagDTO tagDTO, Long userId) {
        Tag tag = tagMapperDTO.convertToEntity(tagDTO);
        Tag oldTag = tagRepository.findById(tag.getId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        if (oldTag.getUser().getId() == userId) {
            oldTag.setName(tag.getName());
            oldTag.setNotes(tag.getNotes());
            tagRepository.save(oldTag);
        }
        return oldTag;
    }

    public void deleteOne(Long id, Long idUser) {
        Tag tag = tagRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        if (tag.getUser().getId() == idUser || idUser == -1L) {
            tag.setNotes(null);
            tag.setGroups(null);
            tagRepository.save(tag);
            tagRepository.delete(tag);
        }
    }

    public void deleteAll(Long idUser) {
        User user = userMapperDTO.convertToEntity(userService.getOne(idUser));

        for (Tag tg : user.getTags()) {
            this.deleteOne(tg.getId(), idUser);
        }
    }

    public TagDTO getOne(Long id) {
        Tag tag = tagRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        return tagMapperDTO.convertToDto(tag);
    }

    public List<TagDTO> getAll() {
        List<TagDTO> lst = new ArrayList<>();

        for (Tag t : tagRepository.findAll()) {
            lst.add(tagMapperDTO.convertToDto(t));
        }
        return lst;
    }

    public Tag saveOne(TagDTO tagDTO, Long idUser) {
        Tag tag = tagMapperDTO.convertToEntity(tagDTO);
        tag.setUser(userMapperDTO.convertToEntity(userService.getOne(idUser)));
        tagRepository.save(tag);
        return tag;
    }

    public void addNote(Long tagId, Note note) {
        Tag tag = tagRepository.findById(tagId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        tag.getNotes().add(note);
        tagRepository.save(tag);
    }

    public void removeNote(Long tagId, Note note) {
        Tag tag = tagRepository.findById(tagId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        tag.getNotes().remove(note);
        tagRepository.save(tag);
    }

    public void addGroup(Long tagId, Group group) {
        Tag tag = tagRepository.findById(tagId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        tag.getGroups().add((group));
        tagRepository.save(tag);
    }

    public void removeGroup(Long tagId, Group group) {
        Tag tag = tagRepository.findById(tagId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        tag.getGroups().remove(group);
        tagRepository.save(tag);
    }
}

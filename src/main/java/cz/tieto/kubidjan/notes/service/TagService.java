package cz.tieto.kubidjan.notes.service;

import cz.tieto.kubidjan.notes.dto.TagDTO;
import cz.tieto.kubidjan.notes.model.Group;
import cz.tieto.kubidjan.notes.model.Note;
import cz.tieto.kubidjan.notes.model.Tag;

import java.util.List;

public interface TagService {

    Tag update(TagDTO tagDTO, Long userId);

    void deleteOne(Long id, Long idUser);

    void deleteAll(Long idUser);

    TagDTO getOne(Long id);

    List<TagDTO> getAll();

    Tag saveOne(TagDTO tagDTO, Long idUser);

    void addNote(Long tagId, Note note);

    void removeNote(Long tagId, Note note);

    void addGroup(Long tagId, Group group);

    void removeGroup(Long tagId, Group group);
}

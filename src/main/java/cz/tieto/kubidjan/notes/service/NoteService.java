package cz.tieto.kubidjan.notes.service;

import cz.tieto.kubidjan.notes.dto.NoteDTO;
import cz.tieto.kubidjan.notes.model.Note;

import java.util.List;

public interface NoteService {

    Note update(NoteDTO noteDTO, Long userId);

    void deleteOne(Long id, Long idUser);

    void deleteAll(Long idUser);

    NoteDTO getOne(Long id, Long userId);

    List<NoteDTO> getAll(Long userId);

    Note saveOne(NoteDTO noteDTO, Long userId, Long groupId);
}

package cz.tieto.kubidjan.notes.service;

import cz.tieto.kubidjan.notes.dto.GroupDTO;
import cz.tieto.kubidjan.notes.model.Group;

import java.util.List;

public interface GroupService {

    Group update(GroupDTO groupDTO, Long idUser);

    void deleteOne(Long id, Long userId);

    void deleteAll(Long userId);

    GroupDTO getOne(Long id, Long userId);

    List<GroupDTO> getAll(Long userId);

    Group saveOne(GroupDTO groupDTO, Long userId);

}

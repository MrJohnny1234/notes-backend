package cz.tieto.kubidjan.notes.service;

import cz.tieto.kubidjan.notes.dto.UserDTO;
import cz.tieto.kubidjan.notes.model.User;

import java.util.List;

public interface UserService {

    User update(UserDTO userDTO);

    void deleteOne(Long id);

    void deleteAll();

    UserDTO getOne(Long id);

    List<UserDTO> getAll();

    User registerNewUserAccount(UserDTO userDTO);


}

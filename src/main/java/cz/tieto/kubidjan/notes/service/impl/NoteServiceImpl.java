package cz.tieto.kubidjan.notes.service.impl;

import cz.tieto.kubidjan.notes.dto.NoteDTO;
import cz.tieto.kubidjan.notes.mapper.GroupMapperDTO;
import cz.tieto.kubidjan.notes.mapper.NoteMapperDTO;
import cz.tieto.kubidjan.notes.mapper.UserMapperDTO;
import cz.tieto.kubidjan.notes.model.Group;
import cz.tieto.kubidjan.notes.model.Note;
import cz.tieto.kubidjan.notes.model.Tag;
import cz.tieto.kubidjan.notes.model.User;
import cz.tieto.kubidjan.notes.repository.NoteRepository;
import cz.tieto.kubidjan.notes.service.GroupService;
import cz.tieto.kubidjan.notes.service.NoteService;
import cz.tieto.kubidjan.notes.service.TagService;
import cz.tieto.kubidjan.notes.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NoteServiceImpl implements NoteService {


    private final NoteRepository noteRepository;
    private final GroupService groupService;
    private final UserService userService;
    private final TagService tagService;
    private final UserMapperDTO userMapperDTO;
    private final NoteMapperDTO noteMapperDTO;
    private final GroupMapperDTO groupMapperDTO;


    public Note update(NoteDTO noteDTO, Long userId) {
        Note note = noteMapperDTO.convertToEntity(noteDTO);
        Note oldNote = noteRepository.findById(note.getId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        if (oldNote.getGroup().getUser().getId() == userId) {
            oldNote.setName(note.getName());
            oldNote.setText(note.getText());
            oldNote.setGroup(note.getGroup());
            oldNote.setTags(note.getTags());
            noteRepository.save(oldNote);
        }
        return oldNote;
    }


    public void deleteOne(Long id, Long idUser) {
        Note note = noteRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        if (note.getGroup().getUser().getId() != idUser) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This is not your group");
        } else {
            for (Tag tag : note.getTags()) {
                if (tag.getNotes().size() == 1 && (tag.getGroups().size() == 0 || tag.getGroups() == null)) {
                    tagService.deleteOne(tag.getId(), -1L);
                } else {
                    tagService.removeNote(tag.getId(), note);
                }
            }
            noteRepository.delete(note);
        }
    }

    public void deleteAll(Long idUser) {
        User user = userMapperDTO.convertToEntity(userService.getOne(idUser));
        for (Group g : user.getGroups()) {
            for (Note note : g.getNotes()) {
                deleteOne(note.getId(), idUser);
            }
        }
    }


    public NoteDTO getOne(Long id, Long userId) {
        Note note = noteRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        if (note.getGroup().getUser().getId() == userId) {
            return noteMapperDTO.convertToDto(note);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This is not your note");
        }
    }

    public List<NoteDTO> getAll(Long userId) {
        List<NoteDTO> lst = new ArrayList<>();

        for (Note n : noteRepository.findAll()) {
            if (n.getGroup().getUser().getId() == userId) {
                lst.add(noteMapperDTO.convertToDto(n));
            }
        }
        return lst;
    }

    public Note saveOne(NoteDTO noteDTO, Long userId, Long groupId) {
        Note note = noteMapperDTO.convertToEntity(noteDTO);
        note.setGroup(groupMapperDTO.convertToEntity(groupService.getOne(groupId, userId)));
        noteRepository.save(note);
        //tagService.addNote(1L, note);
        return note;
    }
}

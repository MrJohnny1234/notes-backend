package cz.tieto.kubidjan.notes.service.impl;

import cz.tieto.kubidjan.notes.dto.GroupDTO;
import cz.tieto.kubidjan.notes.mapper.GroupMapperDTO;
import cz.tieto.kubidjan.notes.mapper.UserMapperDTO;
import cz.tieto.kubidjan.notes.model.Group;
import cz.tieto.kubidjan.notes.model.Tag;
import cz.tieto.kubidjan.notes.model.User;
import cz.tieto.kubidjan.notes.repository.GroupRepository;
import cz.tieto.kubidjan.notes.service.GroupService;
import cz.tieto.kubidjan.notes.service.TagService;
import cz.tieto.kubidjan.notes.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final UserService userService;
    private final TagService tagService;
    private final GroupMapperDTO groupMapperDTO;
    private final UserMapperDTO userMapperDTO;


    public Group update(GroupDTO groupDTO, Long idUser) {
        Group group = groupMapperDTO.convertToEntity(groupDTO);
        Group oldGroup = groupRepository.findById(group.getId()).
                orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

            if (oldGroup.getUser().getId() == idUser) {
                oldGroup.setName(group.getName());
                oldGroup.setNotes(group.getNotes());
                groupRepository.save(oldGroup);
            }
            return oldGroup;
    }

    public void deleteOne(Long id, Long userId) {
        Group group = groupRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        if (group.getUser().getId() != userId) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This is not your group");
        } else {
            for (Tag tag : group.getTags()) {
                if (tag.getGroups().size() == 1 && (tag.getNotes().size() == 0 || tag.getNotes() == null)) {
                    tagService.deleteOne(tag.getId(), -1L);
                } else {
                    tagService.removeGroup(tag.getId(), group);
                }
            }
            groupRepository.delete(group);
        }
    }

    public void deleteAll(Long userId) {
        User user = userMapperDTO.convertToEntity(userService.getOne(userId));
        for (Group g : user.getGroups()) {
            deleteOne(g.getId(), userId);
        }
    }


    public GroupDTO getOne(Long id, Long userId) {
        Group group = groupRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        if (group.getUser().getId() == userId) {
            return groupMapperDTO.convertToDto(group);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This is not your group");
    }


    public List<GroupDTO> getAll(Long userId) {
        List<GroupDTO> lst = new ArrayList<>();

        for (Group g : groupRepository.findAll()) {
            if (g.getUser().getId() == userId) {
                lst.add(groupMapperDTO.convertToDto(g));
            }
        }
        return lst;
    }

    public Group saveOne(GroupDTO groupDTO, Long userId) {
        Group group = groupMapperDTO.convertToEntity(groupDTO);
        group.setUser(userMapperDTO.convertToEntity(userService.getOne(userId)));
        groupRepository.save(group);
        //tagService.addGroup(1L, group);
        return group;
    }
}
